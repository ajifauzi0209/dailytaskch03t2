package com.kazakimaru.dailytaskch03t2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d("Activity Lifecycle", "OnCreate")
    }

    override fun onStart() {
        super.onStart()
        Log.d("Activity Lifecycle", "OnStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d("Activity Lifecycle", "OnResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d("Activity Lifecycle", "OnPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d("Activity Lifecycle", "OnStop")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("Activity Lifecycle", "OnRestart")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("Activity Lifecycle", "OnDestroy")
    }
}

/*
# Running App
- Ketika app pertama kali di buka: onCreate -> onStart -> onResume
# Kondisi Landscape Mode (Rotasi Layar Otomatis = Aktif)
- Memutarkan layar menjadi posisi Landscape: onPause -> onStop -> onDestroy -> onCreate -> onStart -> onResume
# Kondisi Potrait Mode (Rotasi Layar Otomatis = Aktif)
- Memutarkan layar menjadi posisi Landscape: onPause -> onStop -> onDestroy -> onCreate -> onStart -> onResume

# Bonus
- Layar mati: onPause -> onStop
- Sesudah menghidupkan kembali layar: onRestart -> onStart -> onResume
 */